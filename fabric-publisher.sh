#!/usr/bin/env bash
# @TODO 16h/DEV use kubernetes dude ;)
source ./deployment/.deploy_environment.sh
set -xe
fab -u root -f deployment/fabric/fabfile.py -H ${FABRIC_HOSTS} install configure deploy
MESSAGE="fabric deploy $AHAVISION_PACKAGE_VERSION to ${FABRIC_HOSTS}"
date &>> .deploy.status
echo $MESSAGE >> .deploy.status
git add .
git diff-index --quiet HEAD || git commit -s -m "${MESSAGE}"
git push