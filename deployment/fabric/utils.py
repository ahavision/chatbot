# @TODO 1h/BIZ think about it, maybe need publish all my fabric sources?
import time

import cuisine
import fabric.api
import fabric.colors
import fabric.contrib.files
import fabric.operations


def upload_template_with_permissions(local, remote, **kw):
    with cuisine.mode_sudo():
        mode = kw.pop('mode') if 'mode' in kw else '0644'
        owner = kw.pop('owner') if 'owner' in kw else 'root'
        group = kw.pop('group') if 'group' in kw else 'root'

        if cuisine.file_exists(remote):
            old_md5 = cuisine.file_md5(remote)
        else:
            old_md5 = None
        fabric.contrib.files.upload_template(local, remote, **kw)

        fabric.api.sudo('touch %s' % remote)
        fabric.api.sudo('chown %s:%s %s' % (owner, group, remote))
        fabric.api.sudo('chmod %s %s' % (mode, remote))

        new_md5 = cuisine.file_md5(remote)
    return new_md5 != old_md5


def get_local_md5(local_path):
    with cuisine.mode_local():
        local_md5 = cuisine.file_md5(local_path)
    return local_md5


def upload_file_with_permissions(remote, local, *args, **kw):
    with cuisine.mode_sudo():

        mode = kw.pop('mode') if 'mode' in kw else '0644'
        owner = kw.pop('owner') if 'owner' in kw else 'root'
        group = kw.pop('group') if 'group' in kw else 'root'
        binary = kw.pop('binary') if 'binary' in kw else False

        if not binary:
            fabric.api.local('dos2unix %s' % local)

        local_md5 = get_local_md5(local)
        remote_md5 = cuisine.file_md5(remote) if cuisine.file_exists(remote) else None

        if local_md5 != remote_md5:
            fabric.api.puts(fabric.colors.yellow('%s != %s' % (local_md5, remote_md5)))
            cuisine.file_upload(remote, local, *args, **kw)
            uploaded = True
        else:
            uploaded = False

        fabric.api.sudo('touch %s' % remote)
        fabric.api.sudo('chown %s:%s %s' % (owner, group, remote))
        fabric.api.sudo('chmod %s %s' % (mode, remote))

        return uploaded


def supervisor_reload_group(daemon_group, local,
                            start_cmd='start', restart_cmd='restart',
                            process='.*',
                            force_reload=False, template=None):
    remote = '/etc/supervisor/conf.d/%s.conf' % daemon_group
    if not template:
        updated = upload_file_with_permissions(remote, local, owner='root', group='root', mode='0777')
    else:
        updated = upload_template_with_permissions(
            local, remote, context=template, use_jinja=True, use_sudo=True, backup=False
        )
    with fabric.api.settings(warn_only=True):
        status = fabric.api.sudo('service supervisor status', pty=False)

    if 'inactive' in status or 'failed' in status or 'not running' in status:
        fabric.api.puts('inactive' in status)
        fabric.api.puts('failed' in status)
        fabric.api.puts('not running' in status)
        fabric.api.sudo('service supervisor start')
        wait_service_status('supervisor', 'is running', max_count=5, min_positive=1, sleep_time=1)
    else:
        fabric.api.sudo('supervisorctl reread')
        fabric.api.sudo('supervisorctl update')

    running = fabric.api.sudo('supervisorctl status | grep -E "%s:%s" | grep RUNNING | wc -l' % (daemon_group, process))
    if running == '0':
        fabric.api.sudo('supervisorctl %s %s:%s' % (start_cmd, daemon_group, process.replace('.*', '*')))
    elif force_reload or updated:
        fabric.api.sudo('supervisorctl %s %s:%s' % (restart_cmd, daemon_group, process.replace('.*', '*')))

    errors = fabric.api.sudo(
        'supervisorctl status | grep -E "%s:%s" | grep -E "ERROR|FATAL|STOPPED|EXITED" | wc -l' % (
            daemon_group, process
        )
    )
    if errors != '0':
        fabric.operations.abort('SUPERVISOR group %s, process %s not started' % (daemon_group, process))


def wait_service_status(service_name, expect_service_status, max_count, min_positive, sleep_time, subservice=''):
    countdown = 0
    for i in range(1, max_count + 1):
        time.sleep(sleep_time)
        with fabric.api.settings(warn_only=True):
            current_status = fabric.api.sudo(
                'service %s status %s | sed -r "s/\\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g"' % (
                    service_name, subservice
                )
            )
        if expect_service_status in current_status:
            countdown += 1
        if countdown >= min_positive:
            break

    if countdown < min_positive:
        fabric.operations.abort('%s service still in WRONG status when expected [%s] < %s' % (
            service_name.upper(), expect_service_status, min_positive
        ))
