# -*- coding: utf-8 -*-
# @TODO 40h/DEV DUDE use k8s ;) fabric it's a old school ;)

import os

import cuisine
import fabric.api
import fabric.contrib.files

import utils

package_upgraded = {}


@fabric.api.task
def install():
    # @TODO fucking Pottering with fucking systemd ;)
    # see https://superuser.com/questions/1160025/how-to-solve-ttyname-failed-inappropriate-ioctl-for-device-in-vagrant
    fabric.contrib.files.sed('~/.profile', 'mesg y', 'tty -s && mesg n', use_sudo=True)

    # clickhouse key
    fabric.api.sudo("apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E0C56BD4")
    fabric.api.sudo(
        'echo "deb http://repo.yandex.ru/clickhouse/xenial/ stable main" > /etc/apt/sources.list.d/clickhouse.list'
    )
    fabric.api.sudo(
        'echo "deb [trusted=yes] https://%s@apt.fury.io/ahavision/ /" > /etc/apt/sources.list.d/ahavision.list'
        % os.environ.get("FURY_DEPLOY_TOKEN")
    )
    fabric.api.sudo("apt update")
    cuisine.package_ensure_apt(("clickhouse-server-common", "clickhouse-client","dos2unix"))

    # @TODO remove this after switch into paid plan of Gemfury on make my own private repo ;(
    # package_upgraded['ahavision'] = cuisine.package_ensure_apt("ahavision", update=True)
    deb_file = 'ahavision_%s_linux_amd64.deb' % os.environ.get('AHAVISION_PACKAGE_VERSION')
    if utils.upload_file_with_permissions('/tmp/%s' % deb_file,'dist/%s' % deb_file):
        fabric.api.sudo('dpkg -i /tmp/%s' % deb_file)
        package_upgraded['ahavision'] = True
    cuisine.package_ensure_apt("supervisor")


@fabric.api.task
def configure():
    os.environ.update({'configFile': '/etc/ahavision/config.yml'})
    config_updated = utils.upload_template_with_permissions(
        local='deployment/fabric/templates/etc/ahavision/config.yml.j2',
        remote='/etc/ahavision/config.yml',
        context=os.environ,
        mode='0644',
        use_jinja=True,
        use_sudo=True,
        backup=True,
    )
    clickhouse_updated=False
    for f in ('config', 'users'):
        clickhouse_updated = utils.upload_template_with_permissions(
            local='deployment/fabric/templates/etc/clickhouse-server/%s.xml.j2' % f,
            remote='/etc/clickhouse-server/%s.xml' % f,
            context=os.environ,
            mode='0644',
            use_jinja=True,
            use_sudo=True,
            backup=True,
        ) or clickhouse_updated
        if clickhouse_updated:
            fabric.api.sudo('systemctl reload clickhouse-server')
    utils.wait_service_status('clickhouse-server', 'active (running)', 5, 1, 0.1)
    utils.supervisor_reload_group(
        daemon_group='ahavision',
        local='deployment/fabric/templates/etc/supervisor/conf.d/ahavision.conf.j2',
        template=os.environ,
        force_reload=config_updated or package_upgraded['ahavision']
    )


@fabric.api.task
def deploy():
    pass
