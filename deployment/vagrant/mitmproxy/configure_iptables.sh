#!/usr/bin/env bash
# iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 443 -j REDIRECT --to-port 8080
# iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 80 -j REDIRECT --to-port 8080
# iptables -t mangle -A OUTPUT -o enp0s3 -p tcp --dport 443 -m owner --uid-owner $(id -u mitmproxy) -j REDIRECT --to-port 8080
# iptables -t mangle -A OUTPUT -o enp0s3 -p tcp --dport 80 -j REDIRECT --to-port 8080

id -u mitmproxy || adduser mitmproxy
iptables -t nat -A OUTPUT -m tcp -p tcp --dport 443 -m owner '!' --uid-owner mitmproxy -j REDIRECT --to-port 8080
sudo -H -u mitmproxy mitmproxy --mode transparent

# CLIENT_NET=10.0.2.0/24
# TABLE_ID=777
# MARK=777

# echo "$TABLE_ID     mitmproxy" >> /etc/iproute2/rt_tables
# iptables -t mangle -A PREROUTING -d $CLIENT_NET -j MARK --set-mark $MARK
# iptables -t nat -A PREROUTING -p tcp -s $CLIENT_NET --match multiport --dports 80,443 -j REDIRECT --to-port 8080
# iptables -t mangle -A OUTPUT -o enp0s3 -p tcp --dport 443 -j MARK --set-mark $MARK
# iptables -t nat -A OUTPUT -p tcp --dport 443 -j DNAT --to-destination 127.0.0.1:8080

# ip rule add fwmark $MARK lookup $TABLE_ID
# ip route add local $CLIENT_NET dev lo table $TABLE_ID