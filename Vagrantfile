# -*- mode: ruby -*-
# vi: set ft=ruby :
Vagrant.configure(2) do |config|
	config.vm.box = "ubuntu/bionic64"
	config.vm.box_check_update = false

	config.hostmanager.enabled = true
	config.hostmanager.manage_host = true
	config.hostmanager.ignore_private_ip = false
	config.hostmanager.include_offline = false

	config.vm.provider "virtualbox" do |vb|
		vb.gui = false
		vb.memory = "4096"
		vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/vagrant", "1"]
	end

	config.vm.define :chatbot_develop do |chatbot_develop|
    chatbot_develop.vm.synced_folder "./", "/home/ubuntu/go/src/bitbucket.org/ahavision/chatbot"
    # need for only Vagrant development
    chatbot_develop.vm.synced_folder "../../clickhouse_pro/components", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/components"
    chatbot_develop.vm.synced_folder "../../clickhouse_pro/metrika2clickhouse", "/home/ubuntu/go/src/bitbucket.org/clickhouse_pro/metrika2clickhouse"
    chatbot_develop.vm.synced_folder "../../ahavision/anomalyzer", "/home/ubuntu/go/src/bitbucket.org/ahavision/anomalyzer"
    chatbot_develop.vm.synced_folder "../../../github.com/Slach/Recast-Golang-SDK/", "/home/ubuntu/go/src/github.com/Slach/Recast-Golang-SDK/"

    chatbot_develop.vm.host_name = "vagrant-chatbot-ahavision-com"
    chatbot_develop.hostmanager.aliases = ["vagrant.chatbot.ahavision.com"]
    chatbot_develop.vm.network "private_network", ip: "172.16.2.79"
    chatbot_develop.vm.network "forwarded_port", guest: 9090, host: 8888
    chatbot_develop.vm.provision "shell", inline: <<-SHELL
        set -xeuo pipefail
        echo 'APT::Periodic::Enable "0";' > /etc/apt/apt.conf.d/02periodic
        export DEBIAN_FRONTEND=noninteractive
        export GOPATH=/home/ubuntu/go
        export GOROOT=/usr/lib/go-1.13
        grep -q -F 'export GOPATH=$GOPATH' /home/ubuntu/.bashrc  || echo "export GOPATH=$GOPATH" >> /home/ubuntu/.bashrc
        grep -q -F 'export GOPATH=$GOPATH' /home/vagrant/.bashrc || echo "export GOPATH=$GOPATH" >> /home/vagrant/.bashrc
        grep -q -F 'export GOPATH=$GOPATH' /root/.bashrc         || echo "export GOPATH=$GOPATH" >> /root/.bashrc
        
        grep -q -F 'export GOROOT=$GOROOT' /home/ubuntu/.bashrc  || echo "export GOROOT=$GOROOT" >> /home/ubuntu/.bashrc
        grep -q -F 'export GOROOT=$GOROOT' /home/vagrant/.bashrc || echo "export GOROOT=$GOROOT" >> /home/vagrant/.bashrc
        grep -q -F 'export GOROOT=$GOROOT' /root/.bashrc         || echo "export GOROOT=$GOROOT" >> /root/.bashrc

        apt update
        apt-get install -y apt-transport-https software-properties-common aptitude
        # clickhouse
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E0C56BD4
        add-apt-repository "deb http://repo.yandex.ru/clickhouse/deb/stable/ main/"
        # gophers
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 52B59B1571A79DBC054901C0F6BC817356A3D45E
        add-apt-repository ppa:longsleep/golang-backports
        # docker
        apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8D81803C0EBFCD88
        add-apt-repository "deb https://download.docker.com/linux/ubuntu bionic edge"

        apt-get update
        apt-get install -y golang-1.13
        apt-get install -y docker-ce
        apt-get install -y htop ethtool mc iotop dos2unix libffi-dev libssl-dev libxml2-dev libxslt1-dev libjpeg8-dev zlib1g-dev
        apt-get install -y python-pip python3-pip
        apt-get install -y clickhouse-server-common clickhouse-client

        pip2 install -U pip
        rm -rfv /usr/bin/pip2 || true
        pip2.7 install -U docker-compose
        pip2.7 install -r /vagrant/deployment/fabric/requirements.txt

        pip3 install -U pip
        rm -rfv /usr/bin/pip3 || true
        # pip3.6 install -U https://github.com/mitmproxy/mitmproxy/archive/master.zip

        ln -nvsf /usr/lib/go-1.13/bin/go /bin/go
        ln -nvsf /usr/lib/go-1.13/bin/gofmt /bin/gofmt

        apt-get install -y ruby ruby-dev libffi-dev rpm
        gem install fpm fury

        mkdir -p -m 0700 /root/.ssh/
        cp -fv /vagrant/id_rsa /root/.ssh/id_rsa
        chmod 0600 /root/.ssh/id_rsa
        touch /root/.ssh/known_hosts
        ssh-keygen -R github.com
        ssh-keygen -R bitbucket.org
        ssh-keyscan -H github.com >> /root/.ssh/known_hosts
        ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts

        git config --global url."git@github.com:".insteadOf "https://github.com/"
        git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"

        mkdir -p /etc/ahavision/
        cp -fv /vagrant/deployment/vagrant/ahavision/config.yml /etc/ahavision/config.yml
        cp -fv /vagrant/deployment/vagrant/clickhouse/*.xml /etc/clickhouse-server/
        systemctl restart clickhouse-server

        bash -c "cd /home/ubuntu/go/src/bitbucket.org/ahavision/ && go get -u -t -v -d -tags trace ./..."
        bash -c "cd /home/ubuntu/go/src/bitbucket.org/clickhouse_pro/ && go get -u -t -v -d -tags trace ./..."
        bash -c "cd /home/ubuntu/go/src/bitbucket.org/ahavision/anomalyzer && go mod tidy"
        bash -c "cd /home/ubuntu/go/src/bitbucket.org/ahavision/chatbot && go mod tidy"


        goreleaser_urls=$(curl -sL https://github.com/goreleaser/goreleaser/releases/latest | grep href | grep -E "amd64\\.deb|\\.txt" | cut -d '"' -f 2)
        echo "$goreleaser_urls" > /tmp/goreleaser_urls.txt
        sed -i -e "s/^\\/goreleaser/https:\\/\\/github.com\\/goreleaser/" /tmp/goreleaser_urls.txt
        wget -nv -c -i /tmp/goreleaser_urls.txt
        grep amd64.deb goreleaser_checksums.txt | sha256sum
        dpkg -i $(cat goreleaser*checksums.txt | grep amd64.deb | cut -d " " -f 2-)

        nfpm_urls=$(curl -sL https://github.com/goreleaser/nfpm/releases/latest | grep href | grep -E "amd64\\.deb|\\.txt" | cut -d '"' -f 2)
        echo "$nfpm_urls" > /tmp/nfpm_urls.txt
        sed -i -e "s/^\\/goreleaser/https:\\/\\/github.com\\/goreleaser/" /tmp/nfpm_urls.txt
        wget -nv -c -i /tmp/nfpm_urls.txt
        grep amd64.deb nfpm*checksums.txt | sha256sum
        dpkg -i $(cat nfpm*checksums.txt | grep amd64.deb | cut -d " " -f 2-)

        curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(go env GOPATH)/bin v1.21.0
        go get -u -v github.com/Slach/gettext/go-xgettext

        cd /vagrant && /home/ubuntu/go/bin/golangci-lint run --verbose --fast --timeout 600s ./chatbot/
        set +x

        echo "chatbot_develop PROVISIONING DONE, use folloding scenario for developing"
        echo "#  vagrant ssh chatbot_develop"
        echo "#  sudo bash -c \\\"cd /vagrant && GOPATH=/home/ubuntu/go go run -tags trace main.go \\\""
        echo "for docker build run following command"
        echo "#  cd /vagrant && sudo ./docker-publisher.sh"
        echo "for RPM/DEB build run following command"
        echo "#  cd /vagrant && sudo ./gemfury-publisher.sh"
        echo "for production deployment run following command (after build deb)"
        echo "#  cd /vagrant && sudo ./fabric-publisher.sh"
        echo "Good Luck ;)"
		SHELL
	end

end
