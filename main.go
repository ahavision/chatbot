package main

import (
	"bitbucket.org/ahavision/chatbot/cfg"
	"bitbucket.org/ahavision/chatbot/chatbot"
	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"bitbucket.org/clickhouse_pro/components/chpool"
	"bitbucket.org/clickhouse_pro/components/zkpool"
)

var version = "develop"

func main() {
	config := cfg.NewConfig()
	basecfg.InitConfig(config)
	zkpool.InitZookeeper(&config.ConfigType)
	chpool.InitClickhousePool(&config.ConfigType)
	chpool.DetectAllowDistributed(&config.ConfigType)
	chpool.DetectAllowReplicated(&config.ConfigType)

	chatbot.InitLocalConfig(config)
	chatbot.ConnectDB(config)
	defer chatbot.CloseDB(config)
	chatbot.CreateTables()
	//@TODO 2h/DEV need implementation of Language Detect from chat message?
	chatbot.InitLocalization(config, "en-US")

	web := chatbot.InitGramework(config)
	chatbot.DefineChatbotRoutes(web, config)
	chatbot.InitChatbotListener(web, config)
}
