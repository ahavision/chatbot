FROM golang:alpine AS builder
MAINTAINER Eugene Klimov <bloodjazman@gmail.com>

WORKDIR /chatbot/
VOLUME /var/www/anomalies
VOLUME /etc/cron.d

ENV GOPATH /chatbot/
ENV GOROOT /usr/local/go

COPY ./.golangci.yml /chatbot/
COPY ./ /chatbot/src/bitbucket.org/ahavision/chatbot/
COPY ./id_rsa /root/.ssh/id_rsa
RUN chmod 0600 /root/.ssh/id_rsa

RUN apk add --no-cache --update git openssh-client gcc sqlite sqlite-dev musl-dev
RUN touch /root/.ssh/known_hosts
RUN ssh-keygen -R github.com
RUN ssh-keygen -R bitbucket.org
RUN ssh-keyscan -H github.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -H bitbucket.org >> /root/.ssh/known_hosts
RUN git config --global url."git@github.com:".insteadOf "https://github.com/"
RUN git config --global url."git@bitbucket.org:".insteadOf "https://bitbucket.org/"

RUN mkdir -p /chatbot/bin
RUN cd /chatbot/src/bitbucket.org/ahavision/chatbot && go get -tags "trace" -d -t -v ./...

RUN wget -O - -q https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh| sh -s v1.21.0
RUN go env
RUN cd /chatbot/src/bitbucket.org/ahavision/chatbot/ && /chatbot/bin/golangci-lint run --config=/chatbot/.golangci.yml --fast --timeout=600s --exclude=/go/src/ /chatbot/src/bitbucket.org/ahavision/chatbot/chatbot/

RUN cd /chatbot/src/bitbucket.org/ahavision/chatbot/ && go test -tags "trace" -v bitbucket.org/ahavision/chatbot/chatbot
RUN cd /chatbot/src/bitbucket.org/ahavision/chatbot/ && go build -tags "trace" -o /chatbot/bin/chatbot /chatbot/src/bitbucket.org/ahavision/chatbot/main.go

FROM alpine:latest
ENV DOCKERIZE_VERSION v0.4.0
RUN apk add --no-cache --update ca-certificates openssl \
		&& apk add --no-cache --update gzip sed sqlite \
    && update-ca-certificates \
    && wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

COPY --from=builder /chatbot/bin/chatbot /chatbot/bin/
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /usr/local/go/lib/time/zoneinfo.zip
COPY ./locales/ /locales/
ENTRYPOINT ["/bin/sh","-c"]