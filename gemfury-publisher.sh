#!/usr/bin/env bash
source ./deployment/.deploy_environment.sh
set -xe
go get -v github.com/goreleaser/goreleaser
go get -v github.com/Slach/gettext/go-xgettext

MESSAGE="publish to gemfury $AHAVISION_PACKAGE_VERSION"
date &>> .deploy.status
echo $MESSAGE >> .deploy.status
git add .
git diff-index --quiet HEAD || git commit -s -m "${MESSAGE}"

# need git tag for right goreleaser version
git tag $AHAVISION_PACKAGE_VERSION

$GOPATH/bin/go-xgettext -keyword i18n.Get -keyword-plural i18n.GetN -keyword-contextual i18n.GetC -keyword-plural-contextual i18n.GetNC -msgid-bugs-address=bloodjazman@gmail.com -add-comments -sort-output -output ./locales/en-US/chatbot.po $GOPATH/src/bitbucket.org/ahavision/chatbot/**/*.go
$GOPATH/bin/goreleaser --config .goreleaser.yml --rm-dist --skip-publish --skip-validate
bash -c 'fury push --api-token=${FURY_PUBLISH_TOKEN} ./dist/ahavision_${AHAVISION_PACKAGE_VERSION}_linux_amd64.deb'
# @TODO think about self hosted private repository
# bash -c 'fury push --api-token=${FURY_PUBLISH_TOKEN} ./dist/ahavision_${AHAVISION_PACKAGE_VERSION}_linux_amd64.rpm'
git push