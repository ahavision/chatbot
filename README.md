# Ahavision chatbot
Telegram bot which can notify about anomalies in your AppMetrica and Yandex Metrika data

# Howto install development environment

```bash
git clone
cd chatbot
vagrant up --provision
```
