#!/usr/bin/env bash
echo 1 > /proc/sys/vm/drop_caches
source ./deployment/.deploy_environment.sh
set -xeuo pipefail
docker-compose down
#docker system prune -f
bash -c 'echo "${CANISTER_CLICKHOUSEPRO_PASSWORD}" | docker login -u clickhousepro --password-stdin cloud.canister.io:5000'
docker-compose pull metrika2clickhouse
bash -c 'echo "${CANISTER_AHAVISION_PASSWORD}" | docker login -u ahavision --password-stdin cloud.canister.io:5000'
docker-compose pull anomalyzer

# @TODO 16h/DEV need implementation continuos translation see https://docs.weblate.org/en/latest/admin/deployments.html#docker
# @TODO see https://docs.weblate.org/en/latest/admin/continuous.html?highlight=branch
docker-compose run --rm go-xgettext

docker-compose build chatbot
docker-compose push chatbot

MESSAGE="publish $AHAVISION_PACKAGE_VERSION docker to cloud.canister.io"
date &>> .deploy.status
echo $MESSAGE >> .deploy.status
git add .
git diff-index --quiet HEAD || git commit -s -m "${MESSAGE}"
git push
