package cfg

import (
	"flag"

	"bitbucket.org/clickhouse_pro/components/cfg"
	"os"
	"path"
)

//ChatbotConfigType main section subtree for Chatbot
type ChatbotConfigType struct {
	LocalizationDir string `yaml:"localization_dir"`
	Listen          string `yaml:"listen"`
	BaseURL         string `yaml:"base_url"`
	//@TODO 16h/DEV think about implements over fault tolerance distributed cron see http://dkron.io or https://github.com/aptible/supercronic?
	CronDir                        string `yaml:"cron_dir"`
	DbDSN                          string `yaml:"db_dsn"`
	AnomaliesDir                   string `yaml:"anomalies_dir"`
	RecastRequestToken             string `yaml:"recast_request_token"`
	RecastDeveloperToken           string `yaml:"recast_developer_token"`
	CronMetrika2ClickhouseTemplate string `yaml:"cron_metrika2clickhouse_template"`
	CronAnomalyzerTemplate         string `yaml:"cron_anomalyzer_template"`
}

//ConfigType nested from bitbucket.org/clickhouse_pro/components/cfg ConfigType
type ConfigType struct {
	cfg.ConfigType `yaml:",inline"`
	Chatbot        ChatbotConfigType `yaml:"chatbot"`
}

//NewConfig create empty chatbot config
func NewConfig() *ConfigType {
	return &ConfigType{}
}

//DefineCLIFlags fill config from CLI or default values
func (c *ConfigType) DefineCLIFlags() {
	c.ConfigType.DefineCLIFlags()
	//nolint: gas
	cwd, _ := os.Getwd()

	flag.StringVar(&c.Chatbot.Listen, "chatbot.listen", "0.0.0.0:80", "listen HTTP socket")
	flag.StringVar(&c.Chatbot.DbDSN, "chatbot.db_dsn", "sqlite3:/var/lib/ahavision/chatbot.db", "database DSN")
	flag.StringVar(&c.Chatbot.LocalizationDir, "chatbot.localization_dir", path.Join(cwd, "locales"), "directory")
	flag.StringVar(&c.Chatbot.CronDir, "chatbot.cron_dir", "/etc/cron.d", "crontab files directory")
	flag.StringVar(&c.Chatbot.BaseURL, "chatbot.base_url", "http://chatbot.ahavision.com/anomalies", "report directory for serve html + png format files")
	flag.StringVar(&c.Chatbot.AnomaliesDir, "chatbot.anomalies_dir", "/var/www/anomalies", "report directory for serve html + png format files")
	flag.StringVar(&c.Chatbot.RecastRequestToken, "chatbot.recast_request_token", "", "Recast.ai request token see https://recast.ai/slach/ahavision/settings")
	flag.StringVar(&c.Chatbot.RecastDeveloperToken, "chatbot.recast_developer_token", "", "Recast.ai request token see https://recast.ai/slach/ahavision/settings")
	flag.StringVar(&c.Chatbot.CronMetrika2ClickhouseTemplate, "chatbot.cron_metrika2clickhouse_template", `0 3 * * * root bash -e -c "cd /opt/ahavision/ && metrika2clickhouse %s"`, "crontab template for metrika2clickhouse")
	flag.StringVar(&c.Chatbot.CronAnomalyzerTemplate, "chatbot.cron_anomalyzer_template", `0 4 * * * root bash -e -c "cd /opt/ahavision/ && anomalyzer %s"`, "crontab template for anomalyzer")

}
