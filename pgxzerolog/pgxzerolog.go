package pgxzerolog

import (
	"github.com/jackc/pgx"
	"github.com/rs/zerolog"
)

type Logger struct {
	pgx.Logger
	logger *zerolog.Logger
}

func NewLogger(logger *zerolog.Logger) *Logger {
	return &Logger{logger: logger}
}

func (pl *Logger) Log(level pgx.LogLevel, msg string, data map[string]interface{}) {
	switch level {
	case pgx.LogLevelTrace:
		pl.logger.Debug().Interface("data", data).Msg(msg)
	case pgx.LogLevelDebug:
		pl.logger.Debug().Interface("data", data).Msg(msg)
	case pgx.LogLevelInfo:
		pl.logger.Info().Interface("data", data).Msg(msg)
	case pgx.LogLevelWarn:
		pl.logger.Warn().Interface("data", data).Msg(msg)
	case pgx.LogLevelError:
		pl.logger.Error().Interface("data", data).Msg(msg)
	default:
		pl.logger.Error().Interface("data", data).Msg(msg)
	}
}
