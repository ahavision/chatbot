package chatbot

import (
	"bitbucket.org/ahavision/chatbot/cfg"
	"github.com/lestrrat/go-gettext"
	"sync"
)

var i18nCache = sync.Map{}
var i18n gettext.Locale

//InitLocalization load po files by language
func InitLocalization(config *cfg.ConfigType, language string) {
	var cached bool
	var cachedI18N interface{}

	if cachedI18N, cached = i18nCache.Load(language); !cached {
		i18n = gettext.NewLocale(
			language,
			gettext.WithSource(gettext.NewFileSystemSource(config.Chatbot.LocalizationDir)),
		)
		i18nCache.Store(language, i18n)
	} else {
		i18n = cachedI18N.(gettext.Locale)
	}
}
