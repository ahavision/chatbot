package chatbot

import (
	"bitbucket.org/ahavision/chatbot/cfg"
	"fmt"
	"github.com/rs/zerolog/log"
)

var testConfig = &cfg.ConfigType{
	Chatbot: cfg.ChatbotConfigType{
		DbDSN:   "traced-sqlite3://:memory:",
		CronDir: "/tmp/",
	},
}

func dropTables() {
	tables := []string{
		"chat", "chat_history",
	}
	for _, t := range tables {
		q := fmt.Sprintf("DROP TABLE IF EXISTS %s", t)
		if _, err := db.Exec(q); err != nil {
			log.Fatal().Err(err).Str("q", q).Msg("dropTables error")
		}
	}
}

func setupTestCase() func() {
	testConfig.Debug = true
	testConfig.Console = true
	testConfig.InitLoggers()
	InitLocalConfig(testConfig)
	ConnectDB(testConfig)
	CreateTables()
	return func() {
		dropTables()
		CloseDB(testConfig)
	}
}
