package chatbot

import (
	"database/sql"
	"errors"
	"github.com/rs/zerolog/log"
	"time"
)

//Chat main chat entry model
type Chat struct {
	ChatID                 string
	State                  string
	OAuthApplicationID     string
	OAuthApplicationSecret string
	OAuthConfirmCode       string
	OAuthToken             string `json:"access_token" yaml:"access_token"`
	OAuthRefreshToken      string `json:"refresh_token" yaml:"refresh_token"`
	Created                time.Time
	Updated                time.Time
}

func getChatByID(chatID string) *Chat {
	chat := Chat{}
	row := db.QueryRow(`
		SELECT chat_id,state,oauth_application_id,oauth_application_secret,oauth_confirm_code,oauth_token,oauth_refresh_token,created,updated 
		FROM chat WHERE chat_id=:chatID
	`, sql.NamedArg{Name: "chatID", Value: chatID})
	if row == nil {
		log.Fatal().Msg("db.QueryRow error")
	}
	err := row.Scan(
		&chat.ChatID,
		&chat.State,
		&chat.OAuthApplicationID,
		&chat.OAuthApplicationSecret,
		&chat.OAuthConfirmCode,
		&chat.OAuthToken,
		&chat.OAuthRefreshToken,
		&chat.Created,
		&chat.Updated,
	)
	if err == nil {
		return &chat
	}
	log.Error().Err(err).Msg("db.QueryRow.Scan error")
	return nil
}

func (chat *Chat) saveChatHistoryChange(newState *string) (err error) {
	var res sql.Result
	var affectedRows int64
	var q string
	if chat.ChatID == "" {
		return errors.New("chat.ChatID must be not empty")
	}
	if chat.State != *newState && *newState == "" {
		return errors.New("chat.State must be not empty")
	}
	if chat.State != *newState {
		history := ChatHistory{
			Created:  time.Now(),
			ChatID:   chat.ChatID,
			OldState: chat.State,
			NewState: *newState,
		}
		q = "INSERT INTO chat_history (created,chat_id,old_state,new_state) VALUES(:created,:chatId,:oldState,:newState)"
		res, err = db.Exec(
			q,
			sql.Named("created", history.Created),
			sql.Named("chatId", history.ChatID),
			sql.Named("oldState", history.OldState),
			sql.Named("newState", history.NewState),
		)
		if err == nil {
			affectedRows, err = res.RowsAffected()
		}
		if err != nil || affectedRows == 0 {
			log.Error().
				Err(err).
				Str("q", q).
				Int64("affectedRows", affectedRows).
				Msg("INSERT INTO chat_history saveChatHistoryChange error, please restart chatbot server")
		} else {
			if chat.State != "" {
				q = `
				UPDATE chat SET 
					state=:state, 
					oauth_application_id=:oAuthApplicationId,
					oauth_application_secret=:oAuthApplicationSecret,
					oauth_confirm_code=:oAuthConfirmCode,
					oauth_token=:oAuthToken,
					oauth_refresh_token=:oAuthRefreshToken,
					updated=:updated,
					created=:created
				WHERE chat_id=:chatId`
			} else {
				chat.Created = time.Now()
				q = `
				INSERT INTO chat (
					chat_id, state, oauth_application_id, oauth_application_secret, oauth_confirm_code,
					oauth_token, oauth_refresh_token, created, updated
				) VALUES(
					:chatId, :state, :oAuthApplicationId, :oAuthApplicationSecret, :oAuthConfirmCode,
					:oAuthToken, :oAuthRefreshToken,
					:created, :updated
				)
				`
			}
			chat.State = *newState
			chat.Updated = time.Now()
			res, err = db.Exec(q,
				sql.Named("state", chat.State),
				sql.Named("chatId", chat.ChatID),
				sql.Named("created", chat.Created),
				sql.Named("updated", chat.Updated),
				sql.Named("oAuthApplicationId", chat.OAuthApplicationID),
				sql.Named("oAuthApplicationSecret", chat.OAuthApplicationSecret),
				sql.Named("oAuthConfirmCode", chat.OAuthConfirmCode),
				sql.Named("oAuthToken", chat.OAuthToken),
				sql.Named("oAuthRefreshToken", chat.OAuthRefreshToken),
			)
			if err == nil {
				affectedRows, err = res.RowsAffected()
			}
			if err != nil || affectedRows == 0 {

				log.Error().Str("q", q).Int64("affectedRows", affectedRows).
					Err(err).Msg("INSERT INTO chat saveChatHistoryChange error, please restart chatbot server")
			}
		}
	}
	return err
}
