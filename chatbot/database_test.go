package chatbot

import (
	"testing"
)

func TestCloseDB(t *testing.T) {
	ConnectDB(testConfig)
	CloseDB(testConfig)
	CloseDB(testConfig)
}
