package chatbot

import (
	"bitbucket.org/ahavision/chatbot/cfg"
	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"fmt"
	//@TODO remove this after resolve https://github.com/RecastAI/SDK-Golang/issues/15
	"github.com/Slach/Recast-Golang-SDK/recast"
	//"github.com/RecastAI/SDK-Golang/recast"
	"github.com/rs/zerolog/log"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
)

var cronLock sync.Mutex
var conversationIDsRegexp = regexp.MustCompile(`reporter.recast_conversation_ids=([a-z0-9\-,]+)`)

//@TODO need https://github.com/afex/hystrix-go or https://github.com/rubyist/circuitbreaker or https://github.com/regeda/go.fallback here
func updateCronFiles(config *cfg.ConfigType, chat *Chat, msg *recast.Message) (err error) {
	var appMetrikaIds []string
	var metrikaIds []string
	var fakeIDs string
	var appIDsJSON = basecfg.PartialJSONParsingStruct{
		AppMetrikaIds: make(basecfg.JSONIds, 0),
		MetrikaIds:    make(basecfg.JSONIds, 0),
	}
	return updateCronByAvailableIds(config, chat, msg, &fakeIDs, &appIDsJSON, &metrikaIds, &appMetrikaIds)
}

func updateCronByAvailableIds(config *cfg.ConfigType, chat *Chat, msg *recast.Message, fakeIDs *string, appIDsJSON *basecfg.PartialJSONParsingStruct, metrikaIds, appMetrikaIds *[]string) (err error) {
	config.OAuth.Token = chat.OAuthToken
	config.OAuth.RefreshToken = chat.OAuthRefreshToken
	config.ParseAppIDs(metrikaIds, fakeIDs)
	config.GetAvailableIDs(metrikaIds, appIDsJSON, basecfg.MetrikaURL+"/management/v1/counters", "Metrika")
	if err = updateCronByAppIds("Metrika", metrikaIds, config, chat, msg); err != nil {
		return err
	}

	config.ParseAppIDs(appMetrikaIds, fakeIDs)
	config.GetAvailableIDs(appMetrikaIds, appIDsJSON, basecfg.AppmetrikaURL+"/management/v1/applications", "AppMetrika")
	return updateCronByAppIds("AppMetrika", appMetrikaIds, config, chat, msg)
}

func updateCronByAppIds(appType string, appIDs *[]string, config *cfg.ConfigType, chat *Chat, msg *recast.Message) (err error) {
	for _, appID := range *appIDs {
		if err = updateCronFile(appType, appID, config, chat, msg); err != nil {
			return err
		}
	}
	return nil
}

func updateCronFile(appType string, appID string, config *cfg.ConfigType, chat *Chat, msg *recast.Message) (err error) {
	var conversationIDs []string
	cronLock.Lock()
	defer cronLock.Unlock()

	anomalyzerCronName := getAnomalyzerCronName(config, appType, appID)
	metrika2clickhouseCronName := getMetrika2ClickhouseCronName(config, appType, appID)

	cronData, err := ioutil.ReadFile(anomalyzerCronName)
	if err == nil {
		matches := conversationIDsRegexp.FindStringSubmatch(string(cronData))
		if len(matches) > 1 {
			conversationIDs = strings.Split(matches[1], ",")
		}
		if !strings.Contains(string(cronData), msg.ConversationID) {
			conversationIDs = append(conversationIDs, msg.ConversationID)
		}
	} else {
		conversationIDs = []string{msg.ConversationID}
	}
	var appIDCronTemplate string
	if appType == "AppMetrika" {
		appIDCronTemplate = "-appmetrika.app_ids=" + appID
	} else {
		appIDCronTemplate = "-metrika.app_ids=" + appID
	}

	metrika2clickhouseCron := fmt.Sprintf(
		config.Chatbot.CronMetrika2ClickhouseTemplate,
		strings.Join([]string{
			"-console",
			"-non-interactive",
			"-auto_load_ids=false",
			"-clickhouse.hosts=" + strings.Join(config.Clickhouse.Hosts, ","),
			"-clickhouse.username=" + config.Clickhouse.Username,
			"-clickhouse.password=" + config.Clickhouse.Password,
			"-clickhouse.database=" + config.Clickhouse.Database,
			"-clickhouse.clustername=" + config.Clickhouse.ClusterName,
			"-oauth.application_id=" + chat.OAuthApplicationID,
			"-oauth.application_secret=" + chat.OAuthApplicationSecret,
			"-oauth.token=" + chat.OAuthToken,
			appIDCronTemplate,
		}, " "),
	)

	err = ioutil.WriteFile(metrika2clickhouseCronName, []byte(metrika2clickhouseCron), 0644)

	if err != nil {
		return err
	}

	anomalyzerCron := fmt.Sprintf(
		config.Chatbot.CronAnomalyzerTemplate,
		strings.Join([]string{
			"-console",
			"-non-interactive",
			"-auto_load_ids=false",
			"-clickhouse.hosts=" + strings.Join(config.Clickhouse.Hosts, ","),
			"-clickhouse.username=" + config.Clickhouse.Username,
			"-clickhouse.password=" + config.Clickhouse.Password,
			"-clickhouse.database=" + config.Clickhouse.Database,
			"-clickhouse.clustername=" + config.Clickhouse.ClusterName,
			"-oauth.application_id=" + chat.OAuthApplicationID,
			"-oauth.application_secret=" + chat.OAuthApplicationSecret,
			"-oauth.token=" + chat.OAuthToken,
			appIDCronTemplate,
			"-reporter.types=file,recast",
			"-reporter.recast_request_token=" + config.Chatbot.RecastRequestToken,
			"-reporter.recast_conversation_ids=" + strings.Join(conversationIDs, ","),
			"-reporter.report_dir=" + config.Chatbot.AnomaliesDir,
		}, " "),
	)
	return ioutil.WriteFile(anomalyzerCronName, []byte(anomalyzerCron), 0644)
}

func deleteCronFiles(config *cfg.ConfigType, conversationID string) {
	deleteCronByConversationID(config, "Metrika", conversationID)
	deleteCronByConversationID(config, "AppMetrika", conversationID)
}

//@TODO 4h/DEV think about return error ?
func deleteCronByConversationID(config *cfg.ConfigType, appType string, conversationID string) {
	var cronFiles []string
	var err error
	anomalyzerCronPattern := filepath.Join(config.Chatbot.CronDir, fmt.Sprintf("%s_*_anomalyzer", appType))
	cronFiles, err = filepath.Glob(anomalyzerCronPattern)
	if err != nil {
		log.Error().Err(err).Str("appType", appType).Str("anomalyzerCronPattern", anomalyzerCronPattern).Msg("filepath.Glob failed")
		return
	}
	for _, anomalyzerCronName := range cronFiles {
		cronData, err := ioutil.ReadFile(anomalyzerCronName)
		if err == nil {
			cronData := string(cronData)
			if strings.Contains(cronData, conversationID) {
				var conversationIDs []string
				var i int
				var id string
				var found bool

				matches := conversationIDsRegexp.FindStringSubmatch(cronData)
				if len(matches) > 1 {
					conversationIDs = strings.Split(matches[1], ",")
					for i, id = range conversationIDs {
						if id == conversationID {
							found = true
							break
						}
					}
					if found {
						conversationIDs[i] = conversationIDs[len(conversationIDs)-1]
						conversationIDs[len(conversationIDs)-1] = ""
						conversationIDs = conversationIDs[:len(conversationIDs)-1]
					}

					if len(conversationIDs) > 0 {
						cronData = strings.Replace(cronData, matches[1], strings.Join(conversationIDs, ","), 1)
						if err = ioutil.WriteFile(anomalyzerCronName, []byte(cronData), 0644); err != nil {
							log.Error().Err(err).Str("anomalyzerCronName", anomalyzerCronName).Msg("ioutil.WriteFile failed")
						}
					} else {
						metrika2clickhouseCronName := strings.Replace(anomalyzerCronName, "_anomalyzer", "_metrika2clickhouse", 1)
						if err = os.Remove(metrika2clickhouseCronName); err != nil {
							log.Error().Str("metrika2clickhouseCronName", metrika2clickhouseCronName).Msg("os.Remove failed")
						}
						if err = os.Remove(anomalyzerCronName); err != nil {
							log.Error().Str("anomalyzerCronName", anomalyzerCronName).Msg("os.Remove failed")
						}
					}
				} else {
					log.Error().
						Str("anomalyzerCronName", anomalyzerCronName).Str("cronData", cronData).
						Msgf("%s not found", conversationIDsRegexp.String())
				}
			}
		}
	}

}

func getAnomalyzerCronName(config *cfg.ConfigType, appType, appID string) string {
	return filepath.Join(config.Chatbot.CronDir, fmt.Sprintf("%s_%s_anomalyzer", appType, appID))
}

func getMetrika2ClickhouseCronName(config *cfg.ConfigType, appType, appID string) string {
	return filepath.Join(config.Chatbot.CronDir, fmt.Sprintf("%s_%s_metrika2clickhouse", appType, appID))
}
