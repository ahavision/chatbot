package chatbot

import (
	basecfg "bitbucket.org/clickhouse_pro/components/cfg"
	"bytes"
	"github.com/Slach/Recast-Golang-SDK/recast"
	"io/ioutil"
	"strings"
	"testing"
)

func TestUpdateCronFiles(t *testing.T) {
	tearDown := setupTestCase()
	testConfig.Chatbot.CronDir = "/fakedir"
	var appMetrikaIds []string
	var metrikaIds []string
	var fakeIDs = "fakeId1,fakeId2"
	var appIDsJSON = basecfg.PartialJSONParsingStruct{
		AppMetrikaIds: make(basecfg.JSONIds, 0),
		MetrikaIds:    make(basecfg.JSONIds, 0),
	}

	InitLocalization(testConfig, "en-US")
	defer tearDown()
	chat := &Chat{ChatID: "fakeID"}
	msg := &recast.Message{ConversationID: "fakeConversationID"}
	if err := updateCronByAvailableIds(testConfig, chat, msg, &fakeIDs, &appIDsJSON, &metrikaIds, &appMetrikaIds); err == nil {
		t.Fatal("CronDir=/fakedir should return err!=nil")
	}
	if len(metrikaIds) != strings.Count(fakeIDs, ",")+1 {
		t.Fatalf("CronDir=/fakedir should return len(%v)==%d", metrikaIds, strings.Count(fakeIDs, ",")+1)
	}
	if len(appMetrikaIds) != 0 {
		t.Fatalf("CronDir=/fakedir should return len(%v)==%d", appMetrikaIds, 0)
	}

	testConfig.Chatbot.CronDir = "/tmp"
	fakeContent := []byte("reporter.recast_conversation_ids=fake")
	for _, fakeID := range strings.Split(fakeIDs, ",") {
		for _, appType := range []string{"Metrika", "AppMetrika"} {
			tmpAnomalyzerCronName := getAnomalyzerCronName(testConfig, appType, fakeID)
			tmpMetrika2ClickhouseCronName := getMetrika2ClickhouseCronName(testConfig, appType, fakeID)
			if err := ioutil.WriteFile(tmpAnomalyzerCronName, fakeContent, 0644); err != nil {
				t.Fatal(err)
			}
			if err := updateCronFile(appType, fakeID, testConfig, chat, msg); err != nil {
				t.Fatal(err)
			}
			if savedContent, err := ioutil.ReadFile(tmpMetrika2ClickhouseCronName); err == nil {
				if !bytes.Contains(savedContent, []byte(fakeID)) {
					t.Fatalf("%s should contains %s", savedContent, fakeID)
				}
			} else {
				t.Fatal(err)
			}

		}
		deleteCronFiles(testConfig, msg.ConversationID)
	}

}
