package chatbot

import (
	"bitbucket.org/ahavision/chatbot/cfg"

	//@TODO remove this after resolve https://github.com/RecastAI/SDK-Golang/issues/15
	"github.com/Slach/Recast-Golang-SDK/recast"
	//"github.com/RecastAI/SDK-Golang/recast"

	"github.com/levigross/grequests"
	"github.com/rs/zerolog/log"

	grameworklog "github.com/apex/log"
	grameworklog_json "github.com/apex/log/handlers/json"
	"github.com/gramework/gramework"

	"golang.org/x/oauth2/yandex"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const recastTextType = "text"

var recastConnect = recast.ConnectClient{}
var recastRequest = recast.RequestClient{}
var md5regexp = regexp.MustCompile(`^[a-z0-9]{32}$`)
var confirmCodeRegexp = regexp.MustCompile(`^[0-9]{7}$`)
var localConfig *cfg.ConfigType

//InitGramework create instance gramework.App and init loggers
func InitGramework(config *cfg.ConfigType) (web *gramework.App) {
	web = gramework.New()
	logger := &grameworklog.Logger{
		Handler: grameworklog_json.New(os.Stderr),
	}
	if config.Debug {
		logger.Level = grameworklog.DebugLevel
	}
	web.Logger = logger
	return web
}

//DefineChatbotRoutes serve anomalies html+png and
func DefineChatbotRoutes(web *gramework.App, config *cfg.ConfigType) {
	recastConnect.Token = config.Chatbot.RecastRequestToken
	recastRequest.Token = config.Chatbot.RecastRequestToken

	if err := web.Use(web.CORSMiddleware("*")); err != nil {
		log.Fatal().Err(err).Msg("failed use web.CORSMiddleware")
	}

	web.GET("/", messageHandler)
	web.POST("/", messageHandler)
	staticHandler := web.ServeDirCustom(
		config.Chatbot.AnomaliesDir, 1, false, true, []string{},
	)
	web.GET("/anomalies/*any", staticHandler)

}

//InitChatbotListener init listen socket and store config in chatbot.localConfig
func InitChatbotListener(web *gramework.App, config *cfg.ConfigType) {
	if err := web.ListenAndServe(config.Chatbot.Listen); err != nil {
		log.Fatal().Err(err).Msg("listen socket initialization error")
	}
}

//InitLocalConfig Just save reference to global config into localConfig variable, use in updateCronFiles
func InitLocalConfig(config *cfg.ConfigType) {
	localConfig = config
}

//@TODO 16h/DEV read and UNDERSTAND documentation recast.ai NLP API, see recastRequest
func messageHandler(c *gramework.Context) {
	var msg = &recast.MessageData{}
	var err error
	var chat *Chat
	var newState string
	log.Debug().Bytes("request_body", c.Request.Body()).Msg("message received")
	if err = c.UnJSON(msg); err == nil {
		log.Debug().Interface("msg", msg).Msg("message parsed")
		msg.Message.ChatID = msg.ChatID
		msg.Message.SenderID = msg.SenderID
		chat = getChatByID(strconv.FormatUint(msg.ChatID, 10))
		if chat == nil {
			chat = &Chat{ChatID: strconv.FormatUint(msg.ChatID, 10)}
			chat.sendWelComeMessage(&msg.Message)
			newState = "welcome"
		} else if msg.Message.Attachment.Type == recastTextType && strings.Contains(msg.Message.Attachment.Content, "/help") {
			newState = chat.sendHelpMessage(&msg.Message)
		} else if msg.Message.Attachment.Type == recastTextType && strings.Contains(msg.Message.Attachment.Content, "/reset") {
			newState = chat.sendResetMessage(&msg.Message)
		} else if chat.OAuthApplicationID == "" {
			extractTextByRegexp(
				&msg.Message, md5regexp, &chat.OAuthApplicationID, &newState,
				chat.sendOauthApplicationSecretMessage, chat.sendOauthApplicationIDWrong,
			)
		} else if chat.OAuthApplicationSecret == "" {
			extractTextByRegexp(
				&msg.Message, md5regexp, &chat.OAuthApplicationSecret, &newState,
				chat.sendOauthConfirmCodeMessage, chat.sendOauthApplicationSecretWrong,
			)
		} else if chat.OAuthConfirmCode == "" {
			extractTextByRegexp(
				&msg.Message, confirmCodeRegexp, &chat.OAuthConfirmCode, &newState,
				chat.sendOauthSuccess, chat.sendOauthConfirmCodeWrong,
			)
		}
		err = chat.saveChatHistoryChange(&newState)
	}

	if err != nil {
		log.Error().Err(err).Msg("messageHandler error")
		c.Err500(err)
	}

}

type sendFunctionType func(msg *recast.Message) (newState string)

//nolint: megacheck,ineffassign
func extractTextByRegexp(msg *recast.Message, re *regexp.Regexp, chatField, newState *string, okFunction, wrongFunction sendFunctionType) {
	if msg.Attachment.Type == recastTextType && re.MatchString(strings.Trim(msg.Attachment.Content, " \n\r\t")) {
		*chatField = msg.Attachment.Content
		*newState = okFunction(msg)
	} else {
		*newState = wrongFunction(msg)
	}
}

func (chat *Chat) sendMessages(conversationID, errMsg string, messages ...recast.Component) {
	if err := recastConnect.SendMessage(conversationID, messages...); err != nil {
		log.Error().Err(err).Msg(errMsg)
	}
}

//@TODO need implementation for choose only application
func (chat *Chat) sendOauthSuccess(msg *recast.Message) (newState string) {
	var err error
	var resp *grequests.Response
	resp, err = grequests.Post(
		yandex.Endpoint.TokenURL,
		&grequests.RequestOptions{Data: map[string]string{
			"grant_type":    "authorization_code",
			"code":          chat.OAuthConfirmCode,
			"client_id":     chat.OAuthApplicationID,
			"client_secret": chat.OAuthApplicationSecret,
		}},
	)
	if err != nil || !resp.Ok {
		log.Error().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msg("OAuth Response error")
	} else if err = resp.JSON(&chat); err != nil {
		log.Error().Int("response_status", resp.StatusCode).Str("response_body", resp.String()).Err(err).Msg("OAuth JSON Parsing error")
	}
	if err == nil {

		if err = updateCronFiles(localConfig, chat, msg); err == nil {
			//@TODO 2h/DEV need refactoring for sync updateCronFiles and nextDay3Hour time
			now := time.Now()
			nextY, nextM, nextD := time.Now().AddDate(0, 0, 1).Date()
			nextDay3Hour := time.Date(nextY, nextM, nextD, 3, 0, 0, 0, now.Location())
			answer := recast.NewTextMessage(i18n.Get(
				`
				Initialization successful! You will be receive first anomalies as %s.
				You can use /reset command to delete all settings
				`,
				nextDay3Hour,
			))
			err = recastConnect.SendMessage(msg.ConversationID, answer)
		}
	}

	if err != nil {
		log.Error().Err(err).Msg("sendOauthSuccess error")
		answer := recast.NewTextMessage(i18n.Get(`
			Sorry. Something went wrong! 
			Please send /reset command and try again`,
			yandex.Endpoint.AuthURL, chat.OAuthApplicationID,
		))
		if err = recastConnect.SendMessage(msg.ConversationID, answer); err != nil {
			log.Error().Err(err).Msg("recastConnect.SendMessage oauth_success_error error")
		}
		return "oauth_success_error"
	}
	return "oauth_success"
}

func (chat *Chat) sendOauthConfirmCodeWrong(msg *recast.Message) (newState string) {
	answer := recast.NewTextMessage(i18n.Get(
		`
	Confirmation code is wrong! 
	Please, open %s?response_type=code&client_id=%s&scope=appmetrica:read+metrika:read and send 7-digits confirmation code
	`,
		yandex.Endpoint.AuthURL, chat.OAuthApplicationID,
	))
	chat.sendMessages(msg.ConversationID, "confirm_code_wrong sendMessage error", answer)
	return "confirm_code_wrong"
}
func (chat *Chat) sendOauthConfirmCodeMessage(msg *recast.Message) (newState string) {
	answer := recast.NewTextMessage(i18n.Get(
		`Please, open %s?response_type=code&client_id=%s&scope=appmetrica:read+metrika:read and send 7-digits confirmation code`,
		yandex.Endpoint.AuthURL, chat.OAuthApplicationID,
	))
	chat.sendMessages(msg.ConversationID, "confirm_code_request sendMessage error", answer)
	return "confirm_code_request"
}
func (chat *Chat) sendOauthApplicationSecretWrong(msg *recast.Message) (newState string) {
	answer := recast.NewTextMessage(i18n.Get(`
	Oops, secret must be 32 character length string like a md5 
	Get existing secret from https://oauth.yandex.com/ or create new application https://oauth.yandex.com/client/new
	`))
	chat.sendMessages(msg.ConversationID, "app_secret_wrong sendMessage error", answer)
	return "app_secret_wrong"
}
func (chat *Chat) sendOauthApplicationSecretMessage(msg *recast.Message) (newState string) {
	answer := recast.NewTextMessage(i18n.Get(`
	Please submit Yandex OAuth application secret
	Get existing secret from https://oauth.yandex.com/ or create new application https://oauth.yandex.com/client/new
	`))
	chat.sendMessages(msg.ConversationID, "app_secret_request sendMessage error", answer)
	return "app_secret_request"
}
func (chat *Chat) sendOauthApplicationIDWrong(msg *recast.Message) (newState string) {
	answer := recast.NewTextMessage(i18n.Get(`
	Oops, id must be 32 character length text string (not a picture) like a md5 hash 
	Get existing id from https://oauth.yandex.com/ or create new application https://oauth.yandex.com/client/new
	`))
	chat.sendMessages(msg.ConversationID, "app_id_wrong sendMessage error", getHelpMessages()...)
	chat.sendMessages(msg.ConversationID, "app_id_wrong sendMessage error", answer)
	return "app_id_wrong"
}

func (chat *Chat) sendWelComeMessage(msg *recast.Message) {
	answer := recast.NewTextMessage(i18n.Get(`
	Welcome to AhaVision chatbot
	Please submit Yandex OAuth application id 
	Get existing id from https://oauth.yandex.com/ 
	or create new application here 
	https://oauth.yandex.com/client/new
	`))
	chat.sendMessages(msg.ConversationID, "welcome sendMessage error", getHelpMessages()...)
	chat.sendMessages(msg.ConversationID, "welcome sendMessage error", answer)
}

func (chat *Chat) sendResetMessage(msg *recast.Message) string {

	answer := recast.NewTextMessage(i18n.Get(`
	Your OAuth settings reset complete! 
	Please submit Yandex OAuth application id 
	Get existing id from https://oauth.yandex.com/ 
	or create new application here
	https://oauth.yandex.com/client/new
	`))
	chat.sendMessages(msg.ConversationID, "reset sendMessage error", getHelpMessages()...)
	chat.sendMessages(msg.ConversationID, "reset sendMessage error", answer)
	deleteCronFiles(localConfig, msg.ConversationID)
	chat.OAuthApplicationID = ""
	chat.OAuthApplicationSecret = ""
	chat.OAuthToken = ""
	chat.OAuthRefreshToken = ""
	chat.OAuthConfirmCode = ""
	return "reset"
}

func (chat *Chat) sendHelpMessage(msg *recast.Message) string {
	chat.sendMessages(msg.ConversationID, "help sendMessage error", getHelpMessages()...)
	return "help"
}

func getHelpMessages() []recast.Component {
	return []recast.Component{
		recast.NewCard(i18n.Get(`Create application instruction`), i18n.Get(`with screenshots`)).
			AddImage("https://api.monosnap.com/rpc/file/download?id=x0I7qtNnpS0FKVKG3Zee7ofwMJh6ws"),
		recast.NewCard(i18n.Get(`Set callback URL`), i18n.Get(`with screenshots`)).
			AddImage("https://api.monosnap.com/rpc/file/download?id=7LGRTTNWtmdKsQxfFRtEaj4ptXw6p5"),
		recast.NewCard(i18n.Get(`Set oauth access rights scope`), i18n.Get(`with screenshots`)).
			AddImage("https://api.monosnap.com/rpc/file/download?id=hbinre6NWJQreXPZp08w3mQz7Hp4cp"),
		recast.NewCard(i18n.Get(`Get OAuth ID and SECRET`), i18n.Get(`with screenshots`)).
			AddImage("https://api.monosnap.com/rpc/file/download?id=SNpfjHnlkhLOAt68N3vKjSPTwfuKfT"),
	}
}
