package chatbot

import "time"

//ChatHistory model for save state changing
type ChatHistory struct {
	Created  time.Time
	ChatID   string
	OldState string
	NewState string
}
