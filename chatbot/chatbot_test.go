package chatbot

import (
	"github.com/gramework/gramework"
	"github.com/valyala/fasthttp"
	"testing"
)

func TestMessageHandlerWithFakeCredentials(t *testing.T) {
	tearDown := setupTestCase()
	testConfig.Chatbot.Listen = "127.0.0.1:9997"
	InitLocalization(testConfig, "en-US")
	InitGramework(testConfig)
	defer tearDown()

	g := &gramework.Context{}
	g.RequestCtx = &fasthttp.RequestCtx{}

	testHandleWelcomeMessage(g, t)
	testHandleOAuthApplicationIDWrong(g, t)
	testHandleOAuthApplicationID(g, t)
	testHandleOAuthApplicationSecretWrong(g, t)
	testHandleOAuthApplicationSecret(g, t)
	testHandleConfirmCodeWrong(g, t)
	testHandleConfirmCodeUnsuccessfull(g, t)
	testHandleResetMessage(g, t)
}

func TestMessageHandlerDoubleReset(t *testing.T) {
	tearDown := setupTestCase()
	testConfig.Chatbot.Listen = "127.0.0.1:9997"
	InitLocalization(testConfig, "en-US")
	InitGramework(testConfig)
	defer tearDown()

	g := &gramework.Context{}
	g.RequestCtx = &fasthttp.RequestCtx{}
	testHandleWelcomeMessage(g, t)
	testHandleResetMessage(g, t)
	testHandleResetMessage(g, t)
}

func TestDefineChatbotRoutes(t *testing.T) {
	tearDown := setupTestCase()
	testConfig.Chatbot.Listen = "127.0.0.1:9997"
	InitLocalization(testConfig, "en-US")
	web := InitGramework(testConfig)
	DefineChatbotRoutes(web, testConfig)
	defer tearDown()
}

func testHandleWelcomeMessage(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
		{
		  "message": {
			"__v": 0,
			"participant": "152724d7-95a9-4649-9850-8d7593af272b",
			"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
			"attachment": {
			  "content": "welcome",
			  "type": "text"
			},
			"receivedAt": "2018-02-04T07:59:04.550Z",
			"isActive": true,
			"_id": "c6ff66ec-c102-4d1c-8084-9e1149fb80bf"
		  },
		  "chatId": 777,
		  "senderId": 777,
		  "mentioned": true,
		  "origin": "telegram"
		}
		`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "welcome" {
		t.Fatalf("welcome message wrong handled chat=%v", chat)
	}

}

func testHandleOAuthApplicationIDWrong(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
		{
		  "message": {
			"__v": 0,
			"participant": "152724d7-95a9-4649-9850-8d7593af272b",
			"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
			"attachment": {
			  "content": "wrong_oauth_application_id",
			  "type": "text"
			},
			"receivedAt": "2018-02-04T07:59:04.550Z",
			"isActive": true,
			"_id": "c6ff66ec-c102-4d1c-8084-9e1149fb80bf"
		  },
		  "chatId": 777,
		  "senderId": 777,
		  "mentioned": true,
		  "origin": "telegram"
		}
		`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "app_id_wrong" {
		t.Fatalf("welcome message wrong handled chat=%v", chat)
	}

}

func testHandleOAuthApplicationID(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
			{
			  "message": {
				"__v": 0,
				"participant": "152724d7-95a9-4649-9850-8d7593af272b",
				"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
				"attachment": {
				  "content": "b95743f204f64e8b83b7b8348a456777",
				  "type": "text"
				},
				"receivedAt": "2018-02-04T09:00:41.953Z",
				"isActive": true,
				"_id": "6dd8e786-d05a-41f1-9046-b72a302406f3"
			  },
			  "chatId": 777,
			  "senderId": 777,
			  "mentioned": true,
			  "origin": "telegram"
			}
			`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "app_secret_request" || chat.OAuthApplicationID != "b95743f204f64e8b83b7b8348a456777" {
		t.Fatalf("oauth_application_id message wrong handled chat=%v", chat)
	}
}

func testHandleOAuthApplicationSecretWrong(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
				{
				  "message": {
					"__v": 0,
					"participant": "152724d7-95a9-4649-9850-8d7593af272b",
					"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
					"attachment": {
					  "content": "oauth_secret_wrong",
					  "type": "text"
					},
					"receivedAt": "2018-02-04T09:00:41.953Z",
					"isActive": true,
					"_id": "6dd8e786-d05a-41f1-9046-b72a302406f3"
				  },
				  "chatId": 777,
				  "senderId": 777,
				  "mentioned": true,
				  "origin": "telegram"
				}
				`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "app_secret_wrong" {
		t.Fatalf("oauth_application_secret message wrong handled chat=%v", chat)
	}
}

func testHandleOAuthApplicationSecret(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
				{
				  "message": {
					"__v": 0,
					"participant": "152724d7-95a9-4649-9850-8d7593af272b",
					"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
					"attachment": {
					  "content": "b95743f204f64e8b83b7b8348a456777",
					  "type": "text"
					},
					"receivedAt": "2018-02-04T09:00:41.953Z",
					"isActive": true,
					"_id": "6dd8e786-d05a-41f1-9046-b72a302406f3"
				  },
				  "chatId": 777,
				  "senderId": 777,
				  "mentioned": true,
				  "origin": "telegram"
				}
				`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "confirm_code_request" || chat.OAuthApplicationSecret != "b95743f204f64e8b83b7b8348a456777" {
		t.Fatalf("oauth_application_secret message wrong handled chat=%v", chat)
	}
}

func testHandleConfirmCodeWrong(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
					{
					  "message": {
						"__v": 0,
						"participant": "152724d7-95a9-4649-9850-8d7593af272b",
						"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
						"attachment": {
						  "content": "12345678",
						  "type": "text"
						},
						"receivedAt": "2018-02-04T09:00:41.953Z",
						"isActive": true,
						"_id": "6dd8e786-d05a-41f1-9046-b72a302406f3"
					  },
					  "chatId": 777,
					  "senderId": 777,
					  "mentioned": true,
					  "origin": "telegram"
					}
					`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "confirm_code_wrong" {
		t.Fatalf("confirm_code_wrong message wrong handled, why it's successfull? chat=%v", chat)
	}
}

func testHandleConfirmCodeUnsuccessfull(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
					{
					  "message": {
						"__v": 0,
						"participant": "152724d7-95a9-4649-9850-8d7593af272b",
						"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
						"attachment": {
						  "content": "1234567",
						  "type": "text"
						},
						"receivedAt": "2018-02-04T09:00:41.953Z",
						"isActive": true,
						"_id": "6dd8e786-d05a-41f1-9046-b72a302406f3"
					  },
					  "chatId": 777,
					  "senderId": 777,
					  "mentioned": true,
					  "origin": "telegram"
					}
					`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "oauth_success_error" || chat.OAuthConfirmCode != "1234567" {
		t.Fatalf("oauth_confirmation_code message wrong handled, why it's successfull? chat=%v", chat)
	}
}

func testHandleResetMessage(g *gramework.Context, t *testing.T) {
	g.Request.SetBodyString(`
		{
		  "message": {
			"__v": 0,
			"participant": "152724d7-95a9-4649-9850-8d7593af272b",
			"conversation": "53a99510-7b25-4b5e-b178-f3ae267c8101",
			"attachment": {
			  "content": "/reset",
			  "type": "text"
			},
			"receivedAt": "2018-02-04T07:59:04.550Z",
			"isActive": true,
			"_id": "c6ff66ec-c102-4d1c-8084-9e1149fb80bf"
		  },
		  "chatId": 777,
		  "senderId": 777,
		  "mentioned": true,
		  "origin": "telegram"
		}
		`)
	messageHandler(g)
	chat := getChatByID("777")
	if chat == nil || chat.ChatID != "777" || chat.State != "reset" || chat.OAuthApplicationID != "" {
		t.Fatalf("reset message wrong handled chat=%v", chat)
	}

}
