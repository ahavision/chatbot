package chatbot

import (
	"fmt"
	"strconv"
	"testing"
)

func TestChatModel(t *testing.T) {
	tearDown := setupTestCase()
	defer tearDown()

	chat := &Chat{ChatID: ""}
	newState := "welcome"
	if err := chat.saveChatHistoryChange(&newState); err == nil {
		t.Fatal("empty chat.ChatID can't be instedted")
	}

	chat.ChatID = "123"
	if err := chat.saveChatHistoryChange(&newState); err != nil {
		t.Fatal(err)
	}
	if chat == nil || chat.ChatID != "123" || chat.State != "welcome" {
		t.Fatal("saveChatHistoryChange must return chat.ChatID==123 and chat.State==welcome")
	}

	chat.State = ""
	if err := chat.saveChatHistoryChange(&newState); err == nil {
		t.Fatal("empty chat.State with not empty chat.ChatID should return UNIQUE constrain error")
	}

	chat = getChatByID("123")
	if chat == nil || chat.ChatID != "123" || chat.State != "welcome" {
		t.Fatal("getChatByID must return chat.ChatID==123 and chat.State==welcome")
	}
}

func TestMultipleSelectUpdate(t *testing.T) {
	var chat *Chat
	var i int64
	tearDown := setupTestCase()
	defer tearDown()

	for i = 0; i < 3; i++ {
		chat = getChatByID(strconv.FormatInt(i, 10))
		if chat != nil {
			t.Fatalf("getChatByID(%d) must return nil for not exits chat", i)
		}
	}

	chat = &Chat{ChatID: "123"}
	for i = 0; i < 10; i++ {
		newState := fmt.Sprintf("welcome%d", i)
		if err := chat.saveChatHistoryChange(&newState); err != nil {
			t.Fatal(err)
		}
		if chat.State != newState {
			t.Fatalf("chat.State %s != newState %s", chat.State, newState)
		}
		chat = getChatByID(chat.ChatID)
		if chat == nil || chat.ChatID != "123" || chat.State != newState {
			t.Fatalf("getChatByID must return chat.ChatID==123 and chat.State==%s", newState)
		}
	}

}

func TestEmtpyNewState(t *testing.T) {
	var chat *Chat
	var newState string
	tearDown := setupTestCase()
	defer tearDown()
	newState = "welcome"
	chat = &Chat{ChatID: "123", State: ""}
	if err := chat.saveChatHistoryChange(&newState); err != nil {
		t.Fatal(err)
	}
	if chat.State != newState {
		t.Fatalf("chat.State %s should equal newState %s", chat.State, newState)
	}
	newState = ""
	if err := chat.saveChatHistoryChange(&newState); err == nil {
		t.Fatal("empty newState should return err!=nil")
	}

}
