package chatbot

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/ahavision/chatbot/cfg"
	"github.com/go-sql-driver/mysql"
	pgxStdlib "github.com/jackc/pgx/stdlib"
	"github.com/mattn/go-sqlite3"

	//nolint: gotype
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/log/zerologadapter"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/xo/dburl"
)

var db *sql.DB
var dsn *dburl.URL

//InitDBLoggers set tracing log when config.Debug=true
//nolint: interfacer, varcheck
func InitDBLoggers(config *cfg.ConfigType) {
	if strings.Contains(config.Chatbot.DbDSN, "postgres") {
		connConfig := pgx.ConnConfig{
			Logger: zerologadapter.NewLogger(zerolog.Logger{}),
		}
		if config.Debug {
			connConfig.LogLevel = pgx.LogLevelDebug
		}
		pgxStdlib.RegisterDriverConfig(&pgxStdlib.DriverConfig{
			ConnConfig: connConfig,
		})
	}
	if strings.Contains(config.Chatbot.DbDSN, "traced-sqlite3") {
		tracedExists := false
		for _, d := range sql.Drivers() {
			if d == "traced-sqlite3" {
				tracedExists = true
				break
			}
		}
		if config.Debug && !tracedExists {
			eventMask := sqlite3.TraceStmt | sqlite3.TraceProfile | sqlite3.TraceRow | sqlite3.TraceClose
			driver := sqlite3.SQLiteDriver{
				ConnectHook: func(conn *sqlite3.SQLiteConn) error {
					err := conn.SetTrace(&sqlite3.TraceConfig{
						Callback:        sqliteTraceCallback,
						EventMask:       eventMask,
						WantExpandedSQL: true,
					})
					return err
				},
			}
			sql.Register("traced-sqlite3", &driver)
			dburl.Register(dburl.Scheme{
				Driver:    "traced-sqlite3",
				Generator: dburl.GenOpaque,
				Proto:     dburl.ProtoNone,
				Opaque:    true,
				Aliases:   []string{},
				Override:  "",
			})
		} else {
			config.Chatbot.DbDSN = strings.Replace(config.Chatbot.DbDSN, "traced-sqlite3", "sqlite3", -1)
		}
	}
	if strings.Contains(config.Chatbot.DbDSN, "mysql") {
		if err := mysql.SetLogger(&log.Logger); err != nil {
			log.Error().Err(err).Msg("mysql.SetLogger error")
		}
	}
}

var sqliteEventCodes = map[uint32]string{
	sqlite3.TraceStmt:    "TRACE_STATEMENT",
	sqlite3.TraceRow:     "TRACE_ROW",
	sqlite3.TraceProfile: "TRACE_PROFILE",
	sqlite3.TraceClose:   "TRACE_CLOSE",
}

func sqliteTraceCallback(info sqlite3.TraceInfo) int {
	var dbErrText string
	if info.DBError.Code != 0 || info.DBError.ExtendedCode != 0 {
		dbErrText = fmt.Sprintf("DB error: %#v", info.DBError)
	} else {
		dbErrText = "."
	}
	var expandedText string
	if info.ExpandedSQL != "" && info.ExpandedSQL != info.StmtOrTrigger {
		expandedText = info.ExpandedSQL
	}

	var runTimeText string
	if info.RunTimeNanosec != 0 {
		const nanosPerMillisec = 1000000
		if info.RunTimeNanosec%nanosPerMillisec == 0 {
			runTimeText = fmt.Sprintf("%d ms", info.RunTimeNanosec/nanosPerMillisec)
		} else {
			// unexpected: better than millisecond resolution
			runTimeText = fmt.Sprintf("%d ns", info.RunTimeNanosec)
		}
	}

	var modeText string
	if info.AutoCommit {
		modeText = "-AutoCommit-"
	} else {
		modeText = "+Transaction+"
	}
	//if info.StmtOrTrigger != "" || dbErrText != "." || runTimeText != "" {
	log.Debug().
		Str("Event", sqliteEventCodes[info.EventCode]).Str("modeText", modeText).
		Interface("ConnHandle", info.ConnHandle).
		Interface("StmtHandle", info.StmtHandle).
		Str("StmtOrTrigger", info.StmtOrTrigger).
		Str("expandedText", expandedText).
		Str("runTimeText", runTimeText).
		Msg(dbErrText)

	//}

	return 0
}

//ConnectDB open database connection
func ConnectDB(config *cfg.ConfigType) {
	var err error
	InitDBLoggers(config)

	if dsn, err = dburl.Parse(config.Chatbot.DbDSN); err == nil {
		db, err = sql.Open(dsn.Driver, dsn.DSN)
		if err == nil {
			if strings.Contains(dsn.Driver, "sqlite") {
				db.SetMaxOpenConns(1)
			}
			go func() {
				for {
					log.Debug().Str("db", localConfig.Chatbot.DbDSN).Msg("ping->")
					if pingErr := db.Ping(); pingErr != nil {
						log.Fatal().Err(pingErr).
							Interface("dsn", dsn).
							Interface("db", db).
							Msg("ping database error")
					}
					log.Debug().Str("db", localConfig.Chatbot.DbDSN).Msg("<-pong")
					time.Sleep(60 * time.Second)
				}
			}()
		}
	}
	if err != nil {
		log.Fatal().Err(err).
			Str("config.Chatbot.DbDSN", config.Chatbot.DbDSN).
			Msg("ConnectDB error")
	}

}

//CreateTables crate tables if not exists
func CreateTables() {
	var err error
	if !strings.Contains(dsn.Driver, "sqlite3") {
		createDbSQL := fmt.Sprintf(`CREATE DATABASE IF NOT EXISTS "%s"`, strings.Replace(dsn.Path, "/", "", -1))
		if _, err = db.Exec(createDbSQL); err != nil {
			log.Fatal().Err(err).
				Str("createDbSQL", createDbSQL).
				Interface("dsn", dsn).
				Interface("db", db).
				Msg("create database error")
		}
	}
	createTablesSQL := map[string][]string{
		"mysql": {
			`CREATE TABLE IF NOT EXISTS "chat" (
					chat_id VARCHAR(255) NOT NULL PRIMARY KEY,
					state VARCHAR(32),
					oauth_application_id VARCHAR(255),
					oauth_application_secret VARCHAR(255),
					oauth_confirm_code VARCHAR(255),
					oauth_token VARCHAR(255),
					oauth_refresh_token VARCHAR(255),
					created TIMESTAMP NOT NULL,
					updated TIMESTAMP NOT NULL,
					KEY state_idx (state),
					KEY created_idx (created),
					KEY updated_idx (updated)
			)`,
			`CREATE TABLE IF NOT EXISTS "chat_history" (
					created TIMESTAMP NOT NULL,
					chat_id VARCHAR(255) NOT NULL,
					old_state VARCHAR(255),
					new_state VARCHAR(255),
					KEY new_state_idx (new_state),
					KEY old_state_idx (old_state),
					KEY chat_id_idx (chat_id, created),
					KEY created_idx (created)
			)`,
		},
		"sqlite3": {
			`CREATE TABLE IF NOT EXISTS chat (
					chat_id VARCHAR(255) NOT NULL PRIMARY KEY,
					state VARCHAR(32),
					 
					oauth_application_id VARCHAR(255),
					oauth_application_secret VARCHAR(255),
					oauth_confirm_code VARCHAR(255),
					oauth_token VARCHAR(255),
					oauth_refresh_token VARCHAR(255),
					created TIMESTAMP NOT NULL,
					updated TIMESTAMP NOT NULL
			)`,
			`CREATE INDEX IF NOT EXISTS created_idx ON chat (created)`,
			`CREATE INDEX IF NOT EXISTS updated_idx ON chat (updated)`,
			`CREATE TABLE IF NOT EXISTS chat_history (
					created TIMESTAMP NOT NULL,
					chat_id VARCHAR(255) NOT NULL,
					old_state VARCHAR(255),
					new_state VARCHAR(255)
			)`,
			`CREATE INDEX IF NOT EXISTS chat_history_idx ON chat_history (chat_id, created)`,
			`CREATE INDEX IF NOT EXISTS created_history_idx ON chat_history (created)`,
		},
		"postgresql": {
			`CREATE TABLE IF NOT EXISTS "chat" (
					chat_id VARCHAR(255) NOT NULL PRIMARY KEY,
					 
					oauth_application_id VARCHAR(255),
					oauth_application_secret VARCHAR(255),
					oauth_confirm_code VARCHAR(255),
					oauth_token VARCHAR(255),
					oauth_refresh_token VARCHAR(255),
					created TIMESTAMP NOT NULL,
					updated TIMESTAMP NOT NULL,
					KEY created_idx (created),
					KEY updated_idx (updated)
			)`,
			`CREATE TABLE IF NOT EXISTS "chat_history" (
					created TIMESTAMP NOT NULL,
					chat_id VARCHAR(255) NOT NULL,
					old_state VARCHAR(255),
					new_state VARCHAR(255),
					KEY chat_id_idx (chat_id, created)
					KEY created_idx (created)
			)`,
			`CREATE INDEX "_history_idx" ON "chat_history" (created_at)`,
			`CREATE INDEX "created_history_idx" ON "chat_history" (created_at)`,
		},
	}
	exists := false
	for driver, queries := range createTablesSQL {
		if strings.Contains(dsn.Driver, driver) {
			exists = true
			for _, q := range queries {
				if _, err = db.Exec(q); err != nil {
					log.Fatal().Err(err).Str("q", q).
						Interface("dsn", dsn).
						Interface("db", db).
						Msg("create tables error")
				}
			}

		}
	}
	if !exists {
		log.Fatal().Interface("dsn", dsn).Msg("unsupported sql dialect")
	}
}

//CloseDB close database connection
func CloseDB(config *cfg.ConfigType) {
	var err error
	if err = db.Close(); err != nil {
		log.Fatal().Err(err).
			Str("config.Chatbot.DbDSN", config.Chatbot.DbDSN).
			Msg("CloseDB database error")
	}
}
