module bitbucket.org/ahavision/chatbot

go 1.13

require (
	bitbucket.org/clickhouse_pro/components v0.0.0-20191012175805-8cefd664d0f5
	github.com/Slach/Recast-Golang-SDK v0.0.0-20180213105712-4550656f6cfc
	github.com/apex/log v1.1.1
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/elazarl/goproxy v0.0.0-20191011121108-aa519ddbe484 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gocarina/gocsv v0.0.0-20190927101021-3ecffd272576 // indirect
	github.com/gofrs/uuid v3.2.0+incompatible // indirect
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/gramework/gramework v1.6.2
	github.com/gramework/runtimer v0.0.0-20190401193044-e6c0280ee15a // indirect
	github.com/gramework/utils v0.0.0-20190202181041-3c30a162ea26 // indirect
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.0+incompatible
	github.com/jarcoal/httpmock v1.0.4 // indirect
	github.com/jinzhu/configor v1.1.1 // indirect
	github.com/klauspost/compress v1.8.6 // indirect
	github.com/kshvakov/clickhouse v1.3.11 // indirect
	github.com/lestrrat/go-gettext v0.0.0-20180220092308-b9a4882dd343
	github.com/levigross/grequests v0.0.0-20190908174114-253788527a1a
	github.com/lib/pq v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/mattn/kinako v0.0.0-20170717041458-332c0a7e205a // indirect
	github.com/parnurzeal/gorequest v0.2.16 // indirect
	github.com/rs/zerolog v1.15.0
	github.com/samuel/go-zookeeper v0.0.0-20190923202752-2cc03de413da // indirect
	github.com/shopspring/decimal v0.0.0-20191009025716-f1972eb1d1f5 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/valyala/fasthttp v1.5.0
	github.com/xo/dburl v0.0.0-20191005012637-293c3298d6c0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550 // indirect
	golang.org/x/net v0.0.0-20191011234655-491137f69257 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
